rem
rem Automatic build batch file for Flightgear with osgEarth Integration
rem Jeff Biggs (jeff.biggs@simperative.com)
rem

@echo off


set cxxDebug=-DCMAKE_CXX_FLAGS_DEBUG:STRING="/D_DEBUG /MDd /Zi /Ob0 /Od /RTC1"
set cxxRelease=-DCMAKE_CXX_FLAGS_RELEASE:STRING="/MD /O2 /Ob2 /D NDEBUG"
set cxxMinSizeRel=-DCMAKE_CXX_FLAGS_MINSIZEREL:STRING="/MD /O1 /Ob1 /D NDEBUG"
set cxxRelWithDebInfo=-DCMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING="/MD /Zi /O2 /Ob1 /D NDEBUG"
set cxxRelWithDebInfoNoOpt=-DCMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING="/MD /Zi /Od /Ob1 /D NDEBUG"

rem default is Release
set buildconfig=Release
set cxxoptions=%cxxRelease%


rem has cmd line argument, bypass input choices
if not [%1]==[] goto :processCmdLine

setlocal
	rem Visual studio configuration (Release, Debug, MinSizeRel, RelWithDebInfo, or RelWithDebInfoNoOpt)
	set /p InputChoices=Build Configuration?  A=Release (default), B=Debug, C=MinSizeRel, D=RelWithDebInfo, E=RelWithDebInfoNoOpt
	echo %InputChoices%
	call :executeChoices %InputChoices%
endlocal

:executeChoices 
	if [%1]==[] goto :start
	call :Step%1
	shift
	goto :executeChoices    
goto :start

:StepA
	rem Step A - Release Configuration
	set cxxoptions=%cxxRelease%
	set buildconfig=Release
	goto :start
:StepB
	rem Step B - Debug Configuration
	set cxxoptions=%cxxDebug%
	set buildconfig=Debug
	goto :start
:StepC
	rem Step C - MinSizeRel Configuration
	set cxxoptions=%cxxMinSizeRel%
	set buildconfig=MinSizeRel
	goto :start
:StepD
	rem Step D- RelWithDebInfo Configuration
	set cxxoptions=%cxxRelWithDebInfo%
	set buildconfig=RelWithDebInfo
	goto :start
:StepE
	rem Step E - RelWithDebInfoNoOpt Configuration
	set cxxoptions=%cxxRelWithDebInfoNoOpt%
	set buildconfig=RelWithDebInfo
	goto :start
	
	
:processCmdLine

REM --- Optional command line options ---

rem sets Visual studio configuration (Debug, Release, MinSizeRel, RelWithDebInfo, or RelWithDebInfoNoOpt)

rem set build configuration
set configArg="%1"

if not "%configArg%"=="" (
	
	rem -- set cxx compile option ---
	if %configArg%=="Debug" (
		set cxxoptions=%cxxDebug%
		set buildconfig=Debug
	)
	if %configArg%=="Release" (
		set cxxoptions=%cxxRelease%
		set buildconfig=Release
	)
	if %configArg%=="MinSizeRel" (
		set cxxoptions=%cxxMinSizeRel%
		set buildconfig=MinSizeRel
	)
	if %configArg%=="RelWithDebInfo" (
		set cxxoptions=%cxxRelWithDebInfo%
		set buildconfig=RelWithDebInfo
	)
	if %configArg%=="RelWithDebInfoNoOpt" (
		set cxxoptions=%cxxRelWithDebInfoNoOpt%
		set buildconfig=RelWithDebInfo
	)

)


:start

echo --- Building %buildconfig% configuration, with cxx options: %cxxoptions% ---


echo --- Begin cloning git Repositories ---


@ECHO ON


rem clone OpenSceneGraph
if not exist "OpenSceneGraph" call git clone -v -b OpenSceneGraph-3.2.0 https://github.com/openscenegraph/osg.git OpenSceneGraph

rem clone osgEarth
if not exist "osgEarth" call git clone -v -b OsgEarthNext https://github.com/poweroftwo/osgearth.git osgearth

rem clone simgear
if not exist "simgear" call git clone -v -b OsgEarthNext https://gitlab.com/poweroftwo/simgear-osgearth.git simgear

rem clone flightgear
if not exist "flightgear" call git clone -v -b OsgEarthNext https://gitlab.com/poweroftwo/flightgear-osgearth.git flightgear

rem clone fgrun
if not exist "fgrun" call git clone -v -b OsgEarthNext https://gitlab.com/poweroftwo/fgrun-osgearth.git fgrun

rem clone fgdependencies
if not exist "fgdependencies" call git clone -v -b OsgEarthNext https://simperative@bitbucket.org/simperative/fgdependencies.git fgdependencies

rem copy 3rd party directory from fgdependencies
if not exist "3rdParty.x64" xcopy fgdependencies\3rdParty.x64 3rdParty.x64\ /E /Y /R

rem run boost self extraction
if not exist "boost_1_55_0" fgdependencies\boost\UnzipBoost_1_55_0.exe -oboost_1_55_0

rem clone fgscriptutils
if not exist "fgscriptutils" call git clone -v -b OsgEarthNext https://simperative@bitbucket.org/simperative/fgscriptutils.git fgscriptutils

rem create a temporary directory for downloading files
if not exist "tempfiles" (mkdir tempfiles)

rem download a minimal version of fgdata
if not exist "fgmindata" (

	if not exist "tempfiles\fgmindata.7z" (
		fgscriptutils\wget.exe -O tempfiles\fgmindata.7z --no-check-certificate https://dl.dropboxusercontent.com/u/3868105/flightgear/fgmindata.7z
	)
	rem uncompress fgmindata
	call fgscriptutils\7z.exe x -o./ tempfiles\fgmindata.7z
)


@ECHO OFF

set isReposComplete="false"

rem --- confirm that all repositories have been downloaded ---
if exist "OpenSceneGraph/.git" (
	if exist "osgEarth/.git" (
		if exist "simgear/.git" (
			if exist "flightgear/.git" (
				if exist "fgrun/.git" (
					if exist "fgdependencies/.git" (
						if exist "fgscriptutils/.git" (
							set isReposComplete="true"
						)
					)
				)
			)
		)
	)
)

if not %isReposComplete% == "true" (
	echo "Repositories have not successfully downloaded, please try again!"
	pause
	exit /b
) else (
	echo "Repositories have been Successfully downloaded"
)

@ECHO ON

rem
rem --- All Downloads are Complete ---
rem 
rem --- Ready to build... ---
rem

rem --- set local environment variables ---
set thePath="%CD%"
set OSG_3RDPARTY_DIR=%thePath%\3rdParty.x64
set OSG_DIR=%OSG_3RDPARTY_DIR%
set OSGDIR=%OSG_3RDPARTY_DIR%
set BOOST_DIR=%thePath%\boost_1_55_0
set OSG_ROOT=%thePath%\install\msvc100-64\OpenSceneGraph
set OSGEARTH_ROOT=%thePath%\install\msvc100-64\OpenSceneGraph
set SIMGEAR_DIR=%thePath%\install\msvc100-64\simgear
set FLIGHTGEAR_DIR=%thePath%\install\msvc100-64\flightgear
set CMAKE_ROOT=%thePath%\fgscriptutils\cmake2.8
set vcproj=INSTALL.vcxproj

set path=%CMAKE_ROOT%

rem --- configure OSG with Cmake ---

if not exist "OpenSceneGraph\build\%vcproj%" (
	
	if not exist "OpenSceneGraph\build" mkdir OpenSceneGraph\build\
	pushd OpenSceneGraph\build\

		rem configure cmake and create the Visual studio solution file
		cmake.exe ../ -G "Visual Studio 10 Win64" %cxxoptions% -DCMAKE_INSTALL_PREFIX:FILEPATH=%OSG_ROOT% -DCMAKE_RELWITHDEBINFO_POSTFIX:STRING="" -DACTUAL_3RDPARTY_DIR:FILEPATH=%OSG_3RDPARTY_DIR% -DFREETYPE_INCLUDE_DIR=%OSG_3RDPARTY_DIR%/include/freetype
	popd
)

rem locate MSBuild.exe, ensure version is >= v4.5
for /D %%D in (%SYSTEMROOT%\Microsoft.NET\Framework\v4*) do set msbuild=%%D\MSBuild.exe
echo %msbuild%


rem --- Build OSG with visual studio ---

if not exist %OSG_ROOT% (
	pushd OpenSceneGraph\build\
		%msbuild% /m /p:Configuration=%buildconfig% %vcproj% 
	popd
)

if exist %OSG_ROOT% (

	rem --- configure osgEarth with Cmake ---

	if not exist "osgEarth\build\%vcproj%" (
		
		if not exist "osgEarth\build" mkdir osgEarth\build\
		pushd osgEarth\build\

			rem configure cmake and create the Visual studio solution file
			cmake.exe ../ -G "Visual Studio 10 Win64" %cxxoptions% -DCMAKE_INSTALL_PREFIX:FILEPATH=%OSGEARTH_ROOT% -DOSGEARTH_USE_QT:BOOL=OFF -DOSG_DIR:FILEPATH=%OSG_ROOT% -DCMAKE_RELWITHDEBINFO_POSTFIX:STRING="" -DCMAKE_LIBRARY_PATH:FILEPATH=%OSG_3RDPARTY_DIR%/lib -DCMAKE_INCLUDE_PATH:FILEPATH=%OSG_3RDPARTY_DIR%/include 
		popd
	)
		
		
	rem --- Build osgEarth with visual studio ---

	if not exist %OSGEARTH_ROOT%\lib\osgEarth.lib (
		pushd osgEarth\build\
			%msbuild% /m /p:Configuration=%buildconfig% %vcproj%
		popd
	)
)

if exist %OSGEARTH_ROOT% (

	rem --- configure simgear with Cmake ---

	if not exist "simgear\build\%vcproj%" (
		
		if not exist "simgear\build" mkdir simgear\build\
		pushd simgear\build\

			rem configure cmake and create the Visual studio solution file
			cmake.exe ../ -G "Visual Studio 10 Win64" %cxxoptions% -DCMAKE_INSTALL_PREFIX:FILEPATH=%SIMGEAR_DIR% -DBOOST_ROOT:FILEPATH=%BOOST_DIR% -DOSGEARTH_DIR:FILEPATH=%OSGEARTH_ROOT% -DOSG_DIR:FILEPATH=%OSG_ROOT%   -DOPENAL_LIBRARY:FILEPATH=%OSG_3RDPARTY_DIR%/lib/OpenAL32.lib -DZLIB_LIBRARY:PATH=%OSG_3RDPARTY_DIR%/lib/zlib.lib -DZLIB_INCLUDE_DIR:FILEPATH=%OSG_3RDPARTY_DIR%/include
		popd
	)
		
	rem --- Build simgear with visual studio ---

	if not exist %SIMGEAR_DIR%\lib\SimGearCore.lib (
		pushd simgear\build\
			%msbuild% /m /p:Configuration=%buildconfig% %vcproj% 
		popd
	)
)

if exist %SIMGEAR_DIR% (

	rem --- configure flightgear with Cmake ---

	if not exist "flightgear\build\%vcproj%" (
		
		if not exist "flightgear\build" mkdir flightgear\build\
		pushd flightgear\build\

			rem configure cmake and create the Visual studio solution file
			cmake.exe ../ -G "Visual Studio 10 Win64" %cxxoptions% -DCMAKE_INSTALL_PREFIX:FILEPATH=%FLIGHTGEAR_DIR% -DSIMGEAR_DIR:FILEPATH=%SIMGEAR_DIR% -DSIMGEAR_INCLUDE_DIR:FILEPATH=%SIMGEAR_DIR%/include -DSIMGEAR_LIBRARIES:FILEPATH=%SIMGEAR_DIR%/lib -DBOOST_ROOT:FILEPATH=%BOOST_DIR% -DOSGEARTH_DIR:FILEPATH=%OSGEARTH_ROOT% -DOSG_DIR:FILEPATH=%OSG_ROOT% -DMSVC_3RDPARTY_ROOT:FILEPATH=%thePath% -DGLUT_INCLUDE_DIR:FILEPATH=%OSG_3RDPARTY_DIR%/include -DGLUT_glut_LIBRARY:FILEPATH=%OSG_3RDPARTY_DIR%/lib/freeglut.lib -DGDAL_INCLUDE_DIR:FILEPATH=%OSG_3RDPARTY_DIR%/include -DGDAL_LIBRARY:FILEPATH=%OSG_3RDPARTY_DIR%/lib/gdal_i.lib
		popd
	)

	rem --- Build flightgear with visual studio ---

	if not exist %FLIGHTGEAR_DIR%\bin (
		pushd flightgear\build\
			%msbuild% /m /p:Configuration=%buildconfig% %vcproj%
		popd
	)
)

if exist %FLIGHTGEAR_DIR% (

	rem --- configure fgrun with Cmake ---

	if not exist "fgrun\build\%vcproj%" (
		
		if not exist "fgrun\build" mkdir fgrun\build\
		pushd fgrun\build\

			rem configure cmake and create the Visual studio solution file
			cmake.exe ../ -G "Visual Studio 10 Win64" %cxxoptions% -DCMAKE_INSTALL_PREFIX:FILEPATH=%FLIGHTGEAR_DIR% -DMSVC_3RDPARTY_ROOT:FILEPATH=%thePath%  -DBOOST_ROOT:FILEPATH=%BOOST_DIR% -DSIMGEAR_DIR:FILEPATH=%SIMGEAR_DIR% -DENABLE_NLS=OFF	-DOSG_DIR:FILEPATH=%OSG_ROOT%  -DSIMGEAR_DIR:FILEPATH=%SIMGEAR_DIR%
			
		popd
	)

	rem --- Build fgrun with visual studio ---

	if not exist %FLIGHTGEAR_DIR%\bin\fgrun.exe (
		pushd fgrun\build\
			%msbuild% /m /p:Configuration=%buildconfig% %vcproj% 
		popd
	)
)

pause