set root=%cd%

rem set environment path
set path=%root%\3rdParty.x64\bin\;%root%\install\msvc100-64\OpenSceneGraph\bin\

set bin=%root%\install\msvc100-64\flightgear\bin

pushd "%bin%"
	call fgrun.exe --fg-exe=%bin%/fgfs.exe --fg-root=%root%\fgmindata --fg-scenery=%bin%/0 --terrasync-dir=%bin%/0
popd
